// test function paramétrique

function secondary(arg1, operation) {
    console.log(`${arg1} ${operation(2)}`);
}

function primary(arg1) {
    return secondary(arg1, (number) => number * 2);
}

const arg1 = 'Hello!';

primary(arg1);
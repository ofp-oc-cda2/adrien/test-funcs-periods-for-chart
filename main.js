// entrer: data
/* sortie: [
    {
        name: periodLabel,
        field1: number,
        field2: number,
        field3: number,
    }, ...
] */

const data = [[{"_id":"62e272ace749a89d879c6f81","date":"2022-07-28T00:00:00.000Z","value":0,"userId":"6234924b35e8c3add810b079","fieldId":"625808fccc0ef7c39e1c1374","__v":0},{"_id":"62e272b1e749a89d879c6f86","date":"2022-07-27T00:00:00.000Z","value":1,"userId":"6234924b35e8c3add810b079","fieldId":"625808fccc0ef7c39e1c1374","__v":0},{"_id":"62e272b9e749a89d879c6f8b","date":"2022-07-26T00:00:00.000Z","value":2,"userId":"6234924b35e8c3add810b079","fieldId":"625808fccc0ef7c39e1c1374","__v":0},{"_id":"62e272bfe749a89d879c6f90","date":"2022-07-25T00:00:00.000Z","value":4,"userId":"6234924b35e8c3add810b079","fieldId":"625808fccc0ef7c39e1c1374","__v":0},{"_id":"62e272cbe749a89d879c6f95","date":"2022-07-24T00:00:00.000Z","value":8,"userId":"6234924b35e8c3add810b079","fieldId":"625808fccc0ef7c39e1c1374","__v":0},{"_id":"63071f5e7a091eefc27ee816","date":"2022-08-24T00:00:00.000Z","value":5,"userId":"6234924b35e8c3add810b079","fieldId":"625808fccc0ef7c39e1c1374","__v":0},{"_id":"630728b87a091eefc27ee8ca","date":"2022-08-25T00:00:00.000Z","value":20,"userId":"6234924b35e8c3add810b079","fieldId":"625808fccc0ef7c39e1c1374","__v":0},{"_id":"630754157a091eefc27eea23","date":"2022-08-19T00:00:00.000Z","value":0,"userId":"6234924b35e8c3add810b079","fieldId":"625808fccc0ef7c39e1c1374","__v":0},{"_id":"630754207a091eefc27eea28","date":"2022-08-20T00:00:00.000Z","value":10,"userId":"6234924b35e8c3add810b079","fieldId":"625808fccc0ef7c39e1c1374","__v":0},{"_id":"630754277a091eefc27eea2d","date":"2022-08-21T00:00:00.000Z","value":0,"userId":"6234924b35e8c3add810b079","fieldId":"625808fccc0ef7c39e1c1374","__v":0},{"_id":"630754377a091eefc27eea32","date":"2022-08-22T00:00:00.000Z","value":10,"userId":"6234924b35e8c3add810b079","fieldId":"625808fccc0ef7c39e1c1374","__v":0},{"_id":"630dfb7f4208fa93d45aef97","date":"2021-12-31T00:00:00.000Z","value":10,"userId":"6234924b35e8c3add810b079","fieldId":"625808fccc0ef7c39e1c1374","__v":0},{"_id":"630f12ef908d240581154290","date":"2022-08-26T00:00:00.000Z","value":-2,"userId":"6234924b35e8c3add810b079","fieldId":"625808fccc0ef7c39e1c1374","__v":0},{"_id":"6310a58eef2e9b771c346e74","date":"2022-09-01T00:00:00.000Z","value":5,"userId":"6234924b35e8c3add810b079","fieldId":"625808fccc0ef7c39e1c1374","__v":0},{"_id":"6311c1fc8a9621f803ac0c02","date":"2022-09-02T00:00:00.000Z","value":7,"userId":"6234924b35e8c3add810b079","fieldId":"625808fccc0ef7c39e1c1374","__v":0},{"_id":"632f208fe0e4e05656a3d19d","date":"2022-09-18T00:00:00.000Z","value":1,"userId":"6234924b35e8c3add810b079","fieldId":"625808fccc0ef7c39e1c1374","__v":0},{"_id":"632f209be0e4e05656a3d1a2","date":"2022-09-19T00:00:00.000Z","value":2,"userId":"6234924b35e8c3add810b079","fieldId":"625808fccc0ef7c39e1c1374","__v":0},{"_id":"632f20a1e0e4e05656a3d1a7","date":"2022-09-20T00:00:00.000Z","value":4,"userId":"6234924b35e8c3add810b079","fieldId":"625808fccc0ef7c39e1c1374","__v":0},{"_id":"632f20a9e0e4e05656a3d1ac","date":"2022-09-21T00:00:00.000Z","value":8,"userId":"6234924b35e8c3add810b079","fieldId":"625808fccc0ef7c39e1c1374","__v":0},{"_id":"632f20b3e0e4e05656a3d1b1","date":"2022-09-22T00:00:00.000Z","value":16,"userId":"6234924b35e8c3add810b079","fieldId":"625808fccc0ef7c39e1c1374","__v":0},{"_id":"632f20d8e0e4e05656a3d1b6","date":"2022-09-23T00:00:00.000Z","value":24,"userId":"6234924b35e8c3add810b079","fieldId":"625808fccc0ef7c39e1c1374","__v":0},{"_id":"632f20dde0e4e05656a3d1bb","date":"2022-09-24T00:00:00.000Z","value":29,"userId":"6234924b35e8c3add810b079","fieldId":"625808fccc0ef7c39e1c1374","__v":0},{"_id":"63315d47045db5cf7b3da7e0","date":"2022-09-25T00:00:00.000Z","value":30,"userId":"6234924b35e8c3add810b079","fieldId":"625808fccc0ef7c39e1c1374","__v":0},{"_id":"63318901045db5cf7b3da8da","date":"2022-09-26T00:00:00.000Z","value":27,"userId":"6234924b35e8c3add810b079","fieldId":"625808fccc0ef7c39e1c1374","__v":0},{"_id":"6332c9810dc64311c649a489","date":"1970-09-01T00:00:00.000Z","value":99,"userId":"6234924b35e8c3add810b079","fieldId":"625808fccc0ef7c39e1c1374","__v":0},{"_id":"6332c99f0dc64311c649a48e","date":"1969-12-31T00:00:00.000Z","value":999,"userId":"6234924b35e8c3add810b079","fieldId":"625808fccc0ef7c39e1c1374","__v":0},{"_id":"6332c9b80dc64311c649a493","date":"1969-09-24T00:00:00.000Z","value":9999,"userId":"6234924b35e8c3add810b079","fieldId":"625808fccc0ef7c39e1c1374","__v":0},{"_id":"6332fa260dc64311c649a4a7","date":"1969-12-28T00:00:00.000Z","value":1,"userId":"6234924b35e8c3add810b079","fieldId":"625808fccc0ef7c39e1c1374","__v":0},{"_id":"6332fa400dc64311c649a4ac","date":"1969-12-29T00:00:00.000Z","value":2,"userId":"6234924b35e8c3add810b079","fieldId":"625808fccc0ef7c39e1c1374","__v":0},{"_id":"6332fa630dc64311c649a4b1","date":"1970-01-04T00:00:00.000Z","value":2,"userId":"6234924b35e8c3add810b079","fieldId":"625808fccc0ef7c39e1c1374","__v":0},{"_id":"6332fa7c0dc64311c649a4b6","date":"1970-01-05T00:00:00.000Z","value":1,"userId":"6234924b35e8c3add810b079","fieldId":"625808fccc0ef7c39e1c1374","__v":0}],[{"_id":"630751217a091eefc27ee9b6","date":"2022-08-25T00:00:00.000Z","value":10,"userId":"6234924b35e8c3add810b079","fieldId":"62b5a9cdd1b8999538568db6","__v":0},{"_id":"630751297a091eefc27ee9bb","date":"2022-08-24T00:00:00.000Z","value":0,"userId":"6234924b35e8c3add810b079","fieldId":"62b5a9cdd1b8999538568db6","__v":0},{"_id":"630751397a091eefc27ee9c0","date":"2022-08-23T00:00:00.000Z","value":10,"userId":"6234924b35e8c3add810b079","fieldId":"62b5a9cdd1b8999538568db6","__v":0},{"_id":"6307513f7a091eefc27ee9c5","date":"2022-08-22T00:00:00.000Z","value":0,"userId":"6234924b35e8c3add810b079","fieldId":"62b5a9cdd1b8999538568db6","__v":0},{"_id":"630751487a091eefc27ee9ca","date":"2022-08-21T00:00:00.000Z","value":10,"userId":"6234924b35e8c3add810b079","fieldId":"62b5a9cdd1b8999538568db6","__v":0},{"_id":"630751587a091eefc27ee9d2","date":"2022-08-20T00:00:00.000Z","value":0,"userId":"6234924b35e8c3add810b079","fieldId":"62b5a9cdd1b8999538568db6","__v":0},{"_id":"6307515e7a091eefc27ee9d7","date":"2022-08-19T00:00:00.000Z","value":10,"userId":"6234924b35e8c3add810b079","fieldId":"62b5a9cdd1b8999538568db6","__v":0},{"_id":"630f1202908d240581154263","date":"2022-08-26T00:00:00.000Z","value":15,"userId":"6234924b35e8c3add810b079","fieldId":"62b5a9cdd1b8999538568db6","__v":0},{"_id":"630f125b908d24058115426e","date":"2022-08-28T00:00:00.000Z","value":1,"userId":"6234924b35e8c3add810b079","fieldId":"62b5a9cdd1b8999538568db6","__v":0},{"_id":"630f1267908d240581154279","date":"2022-08-29T00:00:00.000Z","value":2,"userId":"6234924b35e8c3add810b079","fieldId":"62b5a9cdd1b8999538568db6","__v":0},{"_id":"630f126d908d24058115427e","date":"2022-08-30T00:00:00.000Z","value":4,"userId":"6234924b35e8c3add810b079","fieldId":"62b5a9cdd1b8999538568db6","__v":0},{"_id":"630f1271908d240581154283","date":"2022-08-31T00:00:00.000Z","value":8,"userId":"6234924b35e8c3add810b079","fieldId":"62b5a9cdd1b8999538568db6","__v":0},{"_id":"6311c20a8a9621f803ac0c09","date":"2022-09-02T00:00:00.000Z","value":1,"userId":"6234924b35e8c3add810b079","fieldId":"62b5a9cdd1b8999538568db6","__v":0},{"_id":"633297de0dc64311c649a3fa","date":"2022-09-14T00:00:00.000Z","value":0.5,"userId":"6234924b35e8c3add810b079","fieldId":"62b5a9cdd1b8999538568db6","__v":0},{"_id":"633297ec0dc64311c649a3ff","date":"2022-09-15T00:00:00.000Z","value":1.5,"userId":"6234924b35e8c3add810b079","fieldId":"62b5a9cdd1b8999538568db6","__v":0},{"_id":"633297fd0dc64311c649a404","date":"2022-09-16T00:00:00.000Z","value":3,"userId":"6234924b35e8c3add810b079","fieldId":"62b5a9cdd1b8999538568db6","__v":0},{"_id":"6332980e0dc64311c649a409","date":"2022-09-17T00:00:00.000Z","value":2.5,"userId":"6234924b35e8c3add810b079","fieldId":"62b5a9cdd1b8999538568db6","__v":0},{"_id":"633298160dc64311c649a40e","date":"2022-09-18T00:00:00.000Z","value":2,"userId":"6234924b35e8c3add810b079","fieldId":"62b5a9cdd1b8999538568db6","__v":0}],[{"_id":"63087fe2506f1f27c5b6817c","date":"2022-08-26T00:00:00.000Z","value":5,"userId":"6234924b35e8c3add810b079","fieldId":"63087fcd506f1f27c5b68173","__v":0},{"_id":"63087fe8506f1f27c5b68181","date":"2022-08-25T00:00:00.000Z","value":10,"userId":"6234924b35e8c3add810b079","fieldId":"63087fcd506f1f27c5b68173","__v":0},{"_id":"630f10e4908d240581154241","date":"2022-08-27T00:00:00.000Z","value":1,"userId":"6234924b35e8c3add810b079","fieldId":"63087fcd506f1f27c5b68173","__v":0},{"_id":"630f10f0908d240581154246","date":"2022-08-28T00:00:00.000Z","value":2,"userId":"6234924b35e8c3add810b079","fieldId":"63087fcd506f1f27c5b68173","__v":0},{"_id":"630f10f4908d24058115424b","date":"2022-08-29T00:00:00.000Z","value":4,"userId":"6234924b35e8c3add810b079","fieldId":"63087fcd506f1f27c5b68173","__v":0},{"_id":"630f10f9908d240581154250","date":"2022-08-30T00:00:00.000Z","value":8,"userId":"6234924b35e8c3add810b079","fieldId":"63087fcd506f1f27c5b68173","__v":0},{"_id":"630f1101908d240581154255","date":"2022-08-31T00:00:00.000Z","value":16,"userId":"6234924b35e8c3add810b079","fieldId":"63087fcd506f1f27c5b68173","__v":0},{"_id":"633298480dc64311c649a41d","date":"2022-09-20T00:00:00.000Z","value":5,"userId":"6234924b35e8c3add810b079","fieldId":"63087fcd506f1f27c5b68173","__v":0},{"_id":"6332984e0dc64311c649a422","date":"2022-09-22T00:00:00.000Z","value":5,"userId":"6234924b35e8c3add810b079","fieldId":"63087fcd506f1f27c5b68173","__v":0},{"_id":"633298540dc64311c649a427","date":"2022-09-24T00:00:00.000Z","value":5,"userId":"6234924b35e8c3add810b079","fieldId":"63087fcd506f1f27c5b68173","__v":0},{"_id":"6332985c0dc64311c649a42c","date":"2022-09-26T00:00:00.000Z","value":5,"userId":"6234924b35e8c3add810b079","fieldId":"63087fcd506f1f27c5b68173","__v":0}]];

// console.log(data);

/* ////////////////////////////////
RELATIVE TO ALL
//////////////////////////////// */

function funcFieldsInObj(periodCode, constFormDate) {
    // entrer: index (=num sem) && constFormDate , sortie: { field: value, field: value, }
    const toReturn = {};
    for (let index1 = 0; index1 < constFormDate.length; index1++) {
        const nameField = `field${index1}`;
        let countField = 0;
        for (let index2 = 0; index2 < constFormDate[index1].length; index2++) {
            const calObj = constFormDate[index1][index2];
            if (calObj.dateFormat === periodCode) {
                countField += calObj.value;
            }
        }
        toReturn[nameField] = countField;
    }
    return toReturn;
};

/* ////////////////////////////////
RELATIVE TO MONTH
//////////////////////////////// */

function labelExtractorMonth(theIndex, lastPeriod) {
    let lastPeriodYear = Number(lastPeriod.slice(0, 4));
    let lastPeriodMonth = Number(lastPeriod.slice(5, 7));
    for (let index = 0; index < theIndex; index++) {
        if (lastPeriodMonth > 1) {
            lastPeriodMonth -= 1;
        } else{
            lastPeriodMonth = 12;
            lastPeriodYear -= 1;
        }
    }
    let lastLabel = `${lastPeriodYear}-${lastPeriodMonth}`;
    if (lastPeriodMonth < 10) {
        lastLabel = `${lastPeriodYear}-0${lastPeriodMonth}`;
    }
    return lastLabel;
};

/* ////////////////////////////////
RELATIVE TO WEEK
//////////////////////////////// */

function dateToNumWeek(dateParse) {
    // entrer: date.parse, sortie: n° sem (sem du 1/01/1970 = 0)
    return Math.trunc(((dateParse - (4 * 24 * 60 * 60 * 1000)) / (7 * 24 * 60 * 60 * 1000)));
};

function dateToString(inDate) {
    // entrer: Date, sortie: '0000-00-00'
    const inDate2 = new Date(inDate);
    const year = inDate.getFullYear();
    let month = inDate.getMonth() + 1; // rappel: janvier = 0
    if (inDate.getMonth() < 9) {
        month = `0${inDate.getMonth() + 1}`
    }
    let day = inDate.getDate();
    if (inDate.getDate() < 10) {
        day = `0${inDate.getDate()}`
    }
    return `${year}-${month}-${day}`;
};

function numWeekToLabel(numweek) {
    // entrer: numweek, sortie: '0000-00-00 - 0000-00-00'
    const numweekInMs = ((numweek+1)*7*24*60*60*1000) - (3 * 24 * 60 * 60 * 1000);
    // lundi
    const theDate = new Date(numweekInMs);
    // dimanche
    const theDateSunday = new Date(numweekInMs + (6*24*60*60*1000));
    // console.log(theDate);
    const theDateStr = `du ${dateToString(theDate)} au ${dateToString(theDateSunday)}`
    return theDateStr;
};

/* ////////////////////////////////
RELATIVE TO funcFormArray
//////////////////////////////// */

function funcFormArray(constFormDate, lastPeriod, numberPeriods, labelExtractor, labelExtractor2, indexInit = 0, arraySens = (a) => a) {
    const arrayFormat = [];
    for (let index = indexInit; index < lastPeriod; index += 1) {
        let lastLabel = labelExtractor(index);
        // search obj in constFormDate with same month code
        // entrer: month code, constFormDate , sortie: { field: value, field: value, }
        const constFieldsInObj = funcFieldsInObj(labelExtractor2(index), constFormDate);
        // merge key obj
        constFieldsInObj.name = lastLabel;
        arrayFormat.push(constFieldsInObj);
    }
    const arrayFormat2 = arraySens(arrayFormat);
    return arrayFormat2;
}

function funcFormArrayYear(constFormDate, lastPeriod, numberPeriods) {
    return funcFormArray(
        constFormDate, // constFormDate
        numberPeriods, // lastPeriod
        numberPeriods, // numberPeriods
        (theIndex) => {
            let lastPeriodYear = Number(lastPeriod);
            for (let index = 0; index < theIndex; index++) {
                lastPeriodYear -= 1;
            }
            return `${lastPeriodYear}`;
        }, // labelExtractor
        (theIndex) => {
            let lastPeriodYear = Number(lastPeriod);
            for (let index = 0; index < theIndex; index++) {
                lastPeriodYear -= 1;
            }
            return `${lastPeriodYear}`;
        } // labelExtractor2
    );
};

function funcFormArrayMonth(constFormDate, lastPeriod, numberPeriods) {
    return funcFormArray(
        constFormDate, // constFormDate
        numberPeriods, // lastPeriod
        numberPeriods, // numberPeriods
        (theIndex) => {
            let lastPeriodYear = Number(lastPeriod.slice(0, 4));
            let lastPeriodMonth = Number(lastPeriod.slice(5, 7));
            for (let index = 0; index < theIndex; index++) {
                if (lastPeriodMonth > 1) {
                    lastPeriodMonth -= 1;
                } else{
                    lastPeriodMonth = 12;
                    lastPeriodYear -= 1;
                }
            }
            let lastLabel = `${lastPeriodYear}-${lastPeriodMonth}`;
            if (lastPeriodMonth < 10) {
                lastLabel = `${lastPeriodYear}-0${lastPeriodMonth}`;
            }
            return lastLabel;
        }, // labelExtractor
        (theIndex) => {
            let lastPeriodYear = Number(lastPeriod.slice(0, 4));
            let lastPeriodMonth = Number(lastPeriod.slice(5, 7));
            for (let index = 0; index < theIndex; index++) {
                if (lastPeriodMonth > 1) {
                    lastPeriodMonth -= 1;
                } else{
                    lastPeriodMonth = 12;
                    lastPeriodYear -= 1;
                }
            }
            let lastLabel = `${lastPeriodYear}-${lastPeriodMonth}`;
            if (lastPeriodMonth < 10) {
                lastLabel = `${lastPeriodYear}-0${lastPeriodMonth}`;
            }
            return lastLabel;
        } // labelExtractor2
    );
};

function funcFormArrayWeek(constFormDate, lastPeriod, numberPeriods) {
    return funcFormArray(
        constFormDate, // constFormDate
        lastPeriod + 1, // lastPeriod
        numberPeriods, // numberPeriods
        (theIndex) => numWeekToLabel(theIndex), // labelExtractor
        (theIndex) => theIndex, // labelExtractor2
        lastPeriod - numberPeriods, // indexInit
        (a) => {
            const toReturn = [];
            for (let index = 0; index < a.length; index++) {
                const element = a[index];
                toReturn.unshift(element);
            }
            return toReturn;
        }
    );
};

/* ////////////////////////////////
RELATIVE TO funcFormDate
//////////////////////////////// */

function funcFormDate(data, dateExtractor) {
    const toReturn = [];
    for (let index = 0; index < data.length; index++) {
        const toReturnField = [];
        for (let index2 = 0; index2 < data[index].length; index2++) {
            const element = data[index][index2];
            const TheDate = new Date(element.date);
            element.dateFormat = dateExtractor(TheDate);
            toReturnField.push(element);
        }
        toReturn.push(toReturnField);
    }
    return toReturn;
}

function funcFormDateWeek(data) {
    return funcFormDate(data, (date) => {
        const TheDateMs = Date.parse(date);
        return dateToNumWeek(TheDateMs);
    });
};

function funcFormDateMonth(data){
    return funcFormDate(data, (date) => {
        if (date.getMonth() + 1 < 10) {
            return `${date.getFullYear()}-0${date.getMonth() + 1}`;
        } else{
            return `${date.getFullYear()}-${date.getMonth() + 1}`;
        }
    });
}

function funcFormDateYear(data){
    return funcFormDate(data, (date) => `${date.getFullYear()}`);
}
/* ////////////////////////////////
RELATIVE TO agregate
//////////////////////////////// */

function agregateYear(data) {
    // ajoute dateFormat
    const constFormDate = funcFormDateYear(data);
    // mise en forme array
    const lastPeriod = '2022'; // généré dynamiquement via ref input
    const numberPeriods = 60; // généré dynamiquement via ref input

    const constFormArray = funcFormArrayYear(constFormDate, lastPeriod, numberPeriods);
    // result
    console.table(constFormArray);
};

function agregateMonth(data) {
    // ajoute dateFormat
    const constFormDate = funcFormDateMonth(data);
    // mise en forme array
    const lastPeriod = '2022-09'; // généré dynamiquement via ref input
    const numberPeriods = 10; // généré dynamiquement via ref input

    const constFormArray = funcFormArrayMonth(constFormDate, lastPeriod, numberPeriods);
    // result
    console.table(constFormArray);
};

function agregateWeek(data) {
    // ajoute dateFormat
    const constFormDate = funcFormDateWeek(data);
    // mise en forme array
    const nowDate = new Date();
    const lastPeriod = dateToNumWeek(Date.parse(nowDate)); // généré dynamiquement via ref input
    const numberPeriods = 10; // généré dynamiquement via ref input

    const constFormArray = funcFormArrayWeek(constFormDate, lastPeriod, numberPeriods);
    // result
    console.table(constFormArray);
};

/* ////////////////////////////////
CALL FUNCTIONS
//////////////////////////////// */

agregateWeek(data);

agregateMonth(data);

agregateYear(data);
